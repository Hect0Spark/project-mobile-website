<!DOCTYPE html>
<head></head>
<html>
<?php
session_start();
$serverhost = $_SERVER['HTTP_HOST'];
$piTestIP   = "192.168.1.38";
$piTestMode = "False";
$loginsuccess = "";
$matched = "";

if ($piTestMode == "True") {
    $piIP = $piTestIP;
} else if ($piTestMode == "False") {
    $piIP = $serverhost;
}



if (isset($_GET['loginsuccess'])) {
    $loginsuccess = $_GET['loginsuccess'];
    if($loginsuccess == "false"){
      $page = "login";
    }
    elseif($loginsuccess == "true"){
    $success                = $_GET['loginsuccess'];
    $username               = $_GET['username'];
    $page                   = "home";
    $_SESSION['login_user'] = $username;
  }
}

elseif (!isset($_SESSION['login_user'])) {
    $page = "login";
} elseif (isset($_GET['page'])) {
    $page = $_GET['page'];

    if ($page == "logout") {
        session_unset();
        session_destroy();
        header('Location: http://' . $piIP . ":5000/Logout?IP=" . $piIP);
        exit;
    }
    if ($page == "logout2") {
        $page = "login";
        session_unset();
        session_destroy();
    }
    if ($page == "settings") {
        $page = "settings";
        $IFTTTSK = $_GET['SecretKey'];
        $IFTTTEN = $_GET['Enabled'];
    }
    if ($page == 'loaddevicePairing'){
        $devicelist = $_GET['devicelist'];
    }
    if ($page == 'loadremote'){
        $devicelist = $_GET['devicelist'];
    }
    if($page == 'remote'){
      $Loadselect = $_GET['Loadselect'];
      $Modeselect = $_GET['Modeselect'];
      $Name = $_GET['Name'];
      $MN1 = $_GET['MN1'];
      $MN2 = $_GET['MN2'];
      $MN3 = $_GET['MN3'];
      $MN4 = $_GET['MN4'];
      $MN5 = $_GET['MN5'];
      $MN6 = $_GET['MN6'];
      $MN7 = $_GET['MN7'];
      $MN8 = $_GET['MN8'];
      $MN9 = $_GET['MN9'];
      $MN10 = $_GET['MN10'];
      $B1 = $_GET['B1'];
      $B2 = $_GET['B2'];
      $B3 = $_GET['B3'];
      $B4 = $_GET['B4'];
      $B5 = $_GET['B5'];
      $B6 = $_GET['B6'];
      $B7 = $_GET['B7'];
      $B8 = $_GET['B8'];
      $B9 = $_GET['B9'];
      $B10 = $_GET['B10'];
    }
    if ($page == "pairing"){
      if(isset($_GET['Loadlist'])){

      }
      elseif(isset($_GET['Loadselect'])){
        $Loadselect = $_GET['Loadselect'];
        $Modeselect = $_GET['Modeselect'];
        $Name = $_GET['Name'];
        $MN1 = $_GET['MN1'];
        $MN2 = $_GET['MN2'];
        $MN3 = $_GET['MN3'];
        $MN4 = $_GET['MN4'];
        $MN5 = $_GET['MN5'];
        $MN6 = $_GET['MN6'];
        $MN7 = $_GET['MN7'];
        $MN8 = $_GET['MN8'];
        $MN9 = $_GET['MN9'];
        $MN10 = $_GET['MN10'];
        $B1 = $_GET['B1'];
        $B2 = $_GET['B2'];
        $B3 = $_GET['B3'];
        $B4 = $_GET['B4'];
        $B5 = $_GET['B5'];
        $B6 = $_GET['B6'];
        $B7 = $_GET['B7'];
        $B8 = $_GET['B8'];
        $B9 = $_GET['B9'];
        $B10 = $_GET['B10'];
      }
      else{
        $Loadselect = "";
        $Modeselect = "1";
        $Name = "";
        $MN1 = "";
        $MN2 = "";
        $MN3 = "";
        $MN4 = "";
        $MN5 = "";
        $MN6 = "";
        $MN7 = "";
        $MN8 = "";
        $MN9 = "";
        $MN10 = "";
        $B1 = "";
        $B2 = "";
        $B3 = "";
        $B4 = "";
        $B5 = "";
        $B6 = "";
        $B7 = "";
        $B8 = "";
        $B9 = "";
        $B10 = "";
      }
    }
} else {
    if (isset($_SESSION['login_user'])) {
        $page = "home";
    } else {
        header('Location: index.php?page=login');
        exit;
    }
}
//By Alix Axel (https://stackoverflow.com/users/89771/alix-axel)
function Redirect($url, $permanent = false)
{
    if (headers_sent() === false) {
        header('Location: ' . $url, true, ($permanent === true) ? 301 : 302);
    }

    exit();
}


?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="w3mobile.css">
<link rel="stylesheet" href="w3-theme-red.css">
<body style="max-width:600px">
  <?php
if (isset($_SESSION['login_user'])):
?>
 <!-- Menu -->
  <nav class="w3-sidebar w3-bar-block w3-card" id="mySidebar">
  <div class="w3-container w3-theme-d2">
    <span onclick="closeSidebar()" class="w3-button w3-display-topright w3-large">X</span>
    <br>
    <div class="w3-padding w3-center">
      <p>
        <font size = "1">
        <?php
    echo "HOST = ", $serverhost;
?>
      </font>
     </p>
      <div>
        Menu
      </div>
    </div>
  </div>
  <a class="w3-bar-item w3-button" href="http://<?php echo $piIP;?>:5000/RemoteLoadlist?IP=<?php echo $piIP; ?>">Remote</a>
  <a class="w3-bar-item w3-button" href="?page=scripts">IFTTT</a>
  <a class="w3-bar-item w3-button" href="?page=pairing">Pairing</a>
  <a class="w3-bar-item w3-button" href="http://<?php echo $piIP;?>:5000/loadSettings?IP=<?php echo $piIP; ?>">Settings</a>
  <a class="w3-bar-item w3-button" href="?page=reboot">Reboot</a>
  <a class="w3-bar-item w3-button" href="?page=logout">Logout</a>
  </nav>
    <?php
endif;
?>
 <header class="w3-bar w3-card w3-theme">
  <button class="w3-bar-item w3-button w3-xxxlarge w3-hover-theme" onclick="openSidebar()">&#9776;</button>
  <h1 class="w3-bar-item">Smart Home Hub</h1>
</header>
  <!-- Menu End -->

  <!-- Main -->

  <!-- login-->
  <?php
if ($page == 'login'):
?>
   <h1>Login</h1>
    <div class="container">
      <form id='login' action='http://<?php echo $piIP; ?>:5000/logincheck?IP=<?php echo $piIP; ?>' method='post' accept-charset='UTF-8'>
        <div>
      <?php if($loginsuccess == "false"){
        echo "The username or password is incorrect";
      } ?>
    </div>
  <label for="username"><b>Username</b></label>
  <input type="text" placeholder="Enter Username" name="username" required>

  <label for="psw"><b>Password</b></label>
  <input type="password" placeholder="Enter Password" name="password" required>
  <button type="submit">Login</button>
</div>
</form>
  <?php
endif;
?>

  <!-- Home-->
  <?php
if ($page == 'home'):
?>
   <h1>Home</h1>
  <?php
endif;
?>
<!---Load Remote --->
<?php
if ($page == 'loadremote'):
?>

<form id=loaddevice action='http://<?php echo $piIP;?>:5000/RemoteLoad?IP=<?php echo $piIP; ?>' method='post' accept-charset='UTF-8'>
  <?php //print_r(array_values($devicelist))  ?>

  <h3>Load</h3>
  <select name="selected" value = "<?php echo htmlentities($Loadselect); ?>">
    <?php foreach($devicelist as $value): ?>
     <option value="<?php print $value ?>" selected><?php print $value ?></option>
   <?php endforeach; ?>
  </select>
  <button type="submit">Load</button>
</form>

<?php
endif;
?>
  <!-- remote-->
  <?php
if ($page == 'remote'):
?>
   <h1>Remote</h1>
     <form action="http://<?php echo $piIP;?>:5000/RemoteLoadlist?IP=<?php echo $piIP; ?>">
    <input type="submit" value="Load Other Device" />
</form>
  <h2><?php echo $Name ?></h2>
<form id='Remote' action="http://<?php echo $piIP;?>:5000/Transmit?IP=<?php echo $piIP; ?>&B1=<?php echo $B1; ?>&B2=<?php echo $B2; ?>&B3=<?php echo $B3; ?>&B4=<?php echo $B4; ?>&B5=<?php echo $B5; ?>&B6=<?php echo $B6; ?>&B7=<?php echo $B7; ?>&B8=<?php echo $B8; ?>&B9=<?php echo $B9; ?>&B10=<?php echo $B10; ?>" method='post' accept-charset='UTF-8'>
   <p>&nbsp;</p>
   <input type="hidden" name="B1" value="<?php echo "$B1" ?>" />
   <input type="hidden" name="B2" value="<?php echo "$B2" ?>" />
   <input type="hidden" name="B3" value="<?php echo "$B3" ?>" />
   <input type="hidden" name="B4" value="<?php echo "$B4" ?>" />
   <input type="hidden" name="B5" value="<?php echo "$B5" ?>" />
   <input type="hidden" name="B6" value="<?php echo "$B6" ?>" />
   <input type="hidden" name="B7" value="<?php echo "$B7" ?>" />
   <input type="hidden" name="B8" value="<?php echo "$B8" ?>" />
   <input type="hidden" name="B9" value="<?php echo "$B9" ?>" />
   <input type="hidden" name="B10" value="<?php echo "$B10" ?>" />
   <input type="hidden" name="MN1" value="<?php echo "$MN1" ?>" />
   <input type="hidden" name="MN2" value="<?php echo "$MN2" ?>" />
   <input type="hidden" name="MN3" value="<?php echo "$MN3" ?>" />
   <input type="hidden" name="MN4" value="<?php echo "$MN4" ?>" />
   <input type="hidden" name="MN5" value="<?php echo "$MN5" ?>" />
   <input type="hidden" name="MN6" value="<?php echo "$MN6" ?>" />
   <input type="hidden" name="MN7" value="<?php echo "$MN7" ?>" />
   <input type="hidden" name="MN8" value="<?php echo "$MN8" ?>" />
   <input type="hidden" name="MN9" value="<?php echo "$MN9" ?>" />
   <input type="hidden" name="MN10" value="<?php echo "$MN10" ?>" />
   <input type="hidden" name="Loadselect" value="<?php echo "$Loadselect" ?>" />
   <input type="hidden" name="Modeselect" value="<?php echo "$Modeselect" ?>" />
   <input type="hidden" name="Name" value="<?php echo "$Name" ?>" />
   <table border="1">
     <tbody>
       <tr>
         <td>Mode</td>
         <td>Mode Name</td>
         <td>Toggle</td>
       </tr>
       <?php
       for($r = 1; $r <= $Modeselect; $r++):?>
       <tr>
         <td><?php echo "$r" ?></td>
         <td><?php echo "${'MN'.$r}"?></td>
         <td><button type="submit" name='Toggle' value="<?php echo "$r"?>">Toggle</button></td>
       </tr>
     <?php endfor ?>
     </tbody>
   </table>
   </form>
   <p>&nbsp;</p>
  <?php
endif;
?>

  <!-- scripts-->
  <?php
if ($page == 'scripts'):
?>
   <h1>IFTTT Webhooks</h1>
  <?php
endif;
?>

  <!-- pairing-->
  <?php
if ($page == 'pairing'):
?>
   <h1>Pairing</h1>
   <div class="container">
     <form id='Pairing' action='http://<?php echo $piIP; ?>:5000/Receive?IP=<?php echo $piIP; ?>' method='post' accept-charset='UTF-8'>
       <h3>Load</h3>

     <button type="submit" name="pair" value="Load">Load Device</button>
   <h3>Name</h3>
   <input type="text" name="Name" value="<?php echo htmlentities($Name); ?>"><br>
   <h3>Modes: </h3>
   <select name="Modeselect" value="<?php echo htmlentities($Modeselect); ?>">
     <option value="1"
     <?php
     if($Modeselect == "1"){
     echo 'selected="selected"';
     }
      ?>>1</option>
     <option value="2"
     <?php
     if($Modeselect == "2"){
        echo 'selected="selected"';
     }
      ?>
      >2</option>
     <option value="3"
     <?php
     if($Modeselect == "3"){
        echo 'selected="selected"';
     }
      ?>
     >3</option>
     <option value="4"
     <?php
     if($Modeselect == "4"){
        echo 'selected="selected"';
     }
      ?>
     >4</option>
     <option value="5"
     <?php
     if($Modeselect == "5"){
        echo 'selected="selected"';
     }
      ?>
     >5</option>
     <option value="6"
     <?php
     if($Modeselect == "6"){
        echo 'selected="selected"';
     }
      ?>
     >6</option>
     <option value="7"
     <?php
     if($Modeselect == "7"){
        echo 'selected="selected"';
     }
      ?>
     >7</option>
     <option value="8"
     <?php
     if($Modeselect == "8"){
        echo 'selected="selected"';
     }
      ?>
     >8</option>
     <option value="9"
     <?php
     if($Modeselect == "9"){
        echo 'selected="selected"';
     }
      ?>
     >9</option>
     <option value="10"
     <?php
     if($Modeselect == "1"){
        echo 'selected="selected"';
     }
      ?>
     >10</option>
   </select>
   <p></p>
   <table border="1">
     <tbody>
       <tr>
         <td>Mode</td>
         <td>Mode Name</td>
         <td>Binary</td>
         <td>Receive</td>
       </tr>
       <tr>
         <td>1</td>
         <td><input type="text" name="MN1" value="<?php echo htmlentities($MN1); ?>"><br></td>
         <td><input type="text" name="B1" value="<?php echo htmlentities($B1); ?>"><br></td>
         <td><button type="submit" name='pair' value="1">Pair</button></td>
       </tr>
       <tr>
         <td>2</td>
         <td><input type="text" name="MN2" value="<?php echo htmlentities($MN2); ?>"><br></td>
         <td><input type="text" name="B2" value="<?php echo htmlentities($B2); ?>"><br></td>
         <td><button type="submit" name='pair' value="2">Pair</button></td>
       </tr>
       <tr>
         <td>3</td>
         <td><input type="text" name="MN3" value="<?php echo htmlentities($MN3); ?>"><br></td>
         <td><input type="text" name="B3" value="<?php echo htmlentities($B3); ?>"><br></td>
         <td><button type="submit" name='pair' value="3">Pair</button></td>
       </tr>
       <tr>
         <td>4</td>
         <td><input type="text" name="MN4" value="<?php echo htmlentities($MN4); ?>"><br></td>
         <td><input type="text" name="B4" value="<?php echo htmlentities($B4); ?>"><br></td>
         <td><button type="submit" name='pair' value="4">Pair</button></td>
       </tr>
       <tr>
         <td>5</td>
         <td><input type="text" name="MN5" value="<?php echo htmlentities($MN5); ?>"><br></td>
         <td><input type="text" name="B5" value="<?php echo htmlentities($B5); ?>"><br></td>
         <td><button type="submit" name='pair' value="5">Pair</button></td>
       </tr>
       <tr>
         <td>6</td>
         <td><input type="text" name="MN6" value="<?php echo htmlentities($MN6); ?>"><br></td>
         <td><input type="text" name="B6" value="<?php echo htmlentities($B6); ?>"><br></td>
         <td><button type="submit" name='pair' value="6">Pair</button></td>
       </tr>
       <tr>
         <td>7</td>
         <td><input type="text" name="MN7" value="<?php echo htmlentities($MN7); ?>"><br></td>
         <td><input type="text" name="B7" value="<?php echo htmlentities($B7); ?>"><br></td>
         <td><button type="submit" name='pair' value="7">Pair</button></td>
       </tr>
       <tr>
         <td>8</td>
         <td><input type="text" name="MN8" value="<?php echo htmlentities($MN8); ?>"><br></td>
         <td><input type="text" name="B8" value="<?php echo htmlentities($B8); ?>"><br></td>
         <td><button type="submit" name='pair' value="8">Pair</button></td>
       </tr>
       <tr>
         <td>9</td>
         <td><input type="text" name="MN9" value="<?php echo htmlentities($MN9); ?>"><br></td>
         <td><input type="text" name="B9" value="<?php echo htmlentities($B9); ?>"><br></td>
         <td><button type="submit" name='pair' value="9">Pair</button></td>
       </tr>
       <tr>
         <td>10</td>
         <td><input type="text" name="MN10" value="<?php echo htmlentities($MN10); ?>"><br></td>
         <td><input type="text" name="B10" value="<?php echo htmlentities($B10); ?>"><br></td>
         <td><button type="submit" name='pair' value="10">Pair</button></td>
       </tr>
     </tbody>
   </table>
<button type="submit" name='pair' value="Save">Save</button>
<button type="submit" name='pair' value="Delete">Delete</button>
</form>
  <?php
endif;
?>

  <!-- settings-->
  <?php
if ($page == 'settings'):
?>
   <h1>Settings</h1>

  <form id='Settings' action='http://<?php echo $piIP; ?>:5000/SavePassword?IP=<?php echo $piIP; ?>' method='post' accept-charset='UTF-8'>
    <h2>Update Password</h2>
    <div><?php if($matched == "false"){ echo "Passwords do not match or old password incorrect";} ?></div>
    <table>
      <tbody>
        <tr>
          <td style="text-align: right;">Old Password:</td>
          <td><input type="password" name="OldPass"></td>
        </tr>
        <tr>
          <td style="text-align: right;">New Password:</td>
          <td><input type="password" name="NewPass"></td>
        </tr>
        <tr>
          <td>Confirm Password:</td>
          <td><input type="password" name="ConPass"</td>
        </tr>
      </tbody>
    </table>
    <button type="submit" onclick="">Save Password</button>
  </form>
  <form id='Settings' action='http://<?php echo $piIP; ?>:5000/SaveIFTTT?IP=<?php echo $piIP; ?>' method='post' accept-charset='UTF-8'>
    <h2>IFTTT Settings</h2>
    <table>
      <tbody>
        <tr>
          <td style="text-align: right;">IFTTT SECRET KEY:</td>
          <td><input type="text" name="SecretKey" value="<?php echo $IFTTTSK ?>"></td>
        </tr>
        <tr>
          <td style="text-align: right;">IFTTT Enabled:</td>
          <td><input type="checkbox" name="IFTTENABLED" value="True"  <?php

          if($IFTTTEN == "True"){
           echo 'checked="checked"';
        }
        elseif($IFTTTEN == "False"){

        }
          ?>></td>
        </tr>
        <tr>
        <td><button type="submit" onclick="">Save IFTTT</button></td>
      </tr>
      </form>
    </tbody>
  </table>
  <?php
endif;
?>

  <!-- reboot -->
  <?php
if ($page == 'reboot'):
?>
   <h1>Reboot</h1>
   <form id='Reboot' action='http://<?php echo $piIP;?>:5000/RebootPi' method='post' accept-charset='UTF-8'>
     <p>Are you sure?</p>
<button type="submit">Reboot</button>
</div>
</form>
  <?php
endif;
?>

<?php
if ($page == 'loaddevicePairing'):
?>
 <form id=loaddevice action='http://<?php echo $piIP;?>:5000/ReceiveLoad?IP=<?php echo $piIP; ?>' method='post' accept-charset='UTF-8'>
   <?php //print_r(array_values($devicelist))  ?>
   <h3>Load</h3>
   <select name="selected" value = "<?php echo htmlentities($Loadselect); ?>">
     <?php foreach($devicelist as $value): ?>
      <option value="<?php print $value ?>" selected><?php print $value ?></option>
    <?php endforeach; ?>
   </select>
   <button type="submit">Load</button>
 </form>
<?php
endif;
?>
 <!-- Main End -->

  <footer class="w3-container w3-theme w3-margin-top">
  <h3>Alexander Bolton - University of Leeds</h3>
</footer>

<script> //for menu
closeSidebar();
function openSidebar() {
    document.getElementById("mySidebar").style.display = "block";
}
function closeSidebar() {
    document.getElementById("mySidebar").style.display = "none";
}
</script>
</body>
</html>
